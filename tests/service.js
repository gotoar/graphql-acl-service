// const mocha = require('mocha')
const chai = require('chai')
const chaiAsPromised = require('chai-as-promised')
chai.use(chaiAsPromised)
const expect = chai.expect

const Service = require('../dist/classes/service').default

class Controller {
  success () {
    return 'success'
  }

  fetch () {
    return {
      customer_id: '2',
      team_id: '1',
      users: ['3', '4'],
      invite: 'abc'
    }
  }

  arr () {
    return [
      {
        id: '3',
        name: 'hey',
        invite: 'hola'
      },
      {
        id: '4',
        name: 'monica',
        invite: 'chau'
      }
    ]
  }
}

const controller = new Controller()

const users = {
  'super': {
    uid: '1',
    role: 'super'
  },
  'admin': {
    uid: '2',
    role: 'admin'
  },
  'admin2': {
    uid: '12',
    role: 'admin'
  },
  'normal1': {
    uid: '3'
  },
  'normal2': {
    uid: '4'
  }
}

const rawData = {
  uid: '3',
  invite: 'abc',
  team_id: '1',
  customer_id: '2'
}

const rawData2 = {
  uid: '3',
  invite: 'asd',
  team_id: '1',
  customer_id: '2'
}

const serviceConfig = {
  exception: Error,
  is: {
    roleField: 'role'
  },
  resolvers: {
    yourself: async (is, user, raw) => is.Role('admin') && user.uid !== raw.customer_id,
    yourself_user: async (is, user, raw) => is.Role() && user.uid !== raw.uid,
    invite_team: async (is, user, raw) => is.Role() && (await controller.fetch(raw.team_id)).invite !== raw.invite,
    super: async (is, user, raw) => !is.Role('super')
  },
  methods: [
    {
      name: 'batchFetch',
      method: controller.success
    },
    {
      name: 'create',
      method: controller.create,
      rules: [
        { type: 'privileged', message: 'Only admins' },
        { type: 'yourself', message: 'You can only create users for yourself' },
        { type: 'yourself_user', message: 'You can only register yourself' }
      ],
      sanitize: true
    },
    {
      name: 'joinTeam',
      method: controller.success,
      rules: [
        { type: 'yourself_user', message: 'You can only update yourself' },
        { type: 'invite_team', message: 'Invalid invite' }
      ]
    }
  ],
  fields: {
    users: [
      { type: 'super', message: 'Only admins' }
    ]
  }
}

describe('Merges resolvers', () => {
  it('should have all the resolvers', async () => {
    const service = new Service(users.super, serviceConfig)
    expect(service.config.resolvers).to.have.property('privileged')
    expect(service.config.resolvers).to.have.property('yourself')
    expect(service.config.resolvers).to.have.property('yourself_user')
    expect(service.config.resolvers).to.have.property('invite_team')
  })
})

describe('Registers methods', () => {
  it('should have all the custom methods registered', async () => {
    const service = new Service(users.super, serviceConfig)
    expect(service.batchFetch).to.be.instanceof(Function)
    expect(service.create).to.be.instanceof(Function)
    expect(service.joinTeam).to.be.instanceof(Function)
  })
})

describe('Given a raw payload and a format, returns the formatted arguments', () => {
  const service = new Service(users.super, serviceConfig)
  const data = {
    property1: 'hey',
    name: 'tom',
    id: '42',
    extra: 'hello',
    nested_object: {
      a: 1,
      b: 2,
      another_level: {
        hey: 'hello'
      }
    },
    nested_array: ['hey', 'monica']
  }

  it('should return no arguments', () => {
    const args = []
    const formatted = service.makeArgs(args, data)
    expect(formatted).to.have.length(0)
  })

  it('should return one argument', () => {
    const args = ['id']
    const formatted = service.makeArgs(args, data)
    expect(formatted).to.have.length(1)
    expect(formatted[0]).to.equal(data['id'])
  })

  it('should return two plain arguments', () => {
    const args = ['id', 'name']
    const formatted = service.makeArgs(args, data)
    expect(formatted).to.have.length(2)
    expect(formatted[0]).to.equal(data['id'])
    expect(formatted[1]).to.equal(data['name'])
  })

  it('should return an argument and the whole payload', () => {
    const args = ['id', '*']
    const formatted = service.makeArgs(args, data)
    expect(formatted).to.have.length(2)
    expect(formatted[0]).to.equal(data['id'])
    expect(formatted[1]).to.be.instanceof(Object)
    expect(formatted[1]).to.deep.equal(data)
  })

  it('should return two arguments and the whole payload', () => {
    const args = ['id', 'extra', '*']
    const formatted = service.makeArgs(args, data)
    expect(formatted).to.have.length(3)
    expect(formatted[0]).to.equal(data['id'])
    expect(formatted[1]).to.equal(data['extra'])
    expect(formatted[2]).to.be.instanceof(Object)
    expect(formatted[2]).to.deep.equal(data)
  })

  it('should return an argument, the whole payload and another argument', () => {
    const args = ['id', '*', 'extra']
    const formatted = service.makeArgs(args, data)
    expect(formatted).to.have.length(3)
    expect(formatted[0]).to.equal(data['id'])
    expect(formatted[1]).to.be.instanceof(Object)
    expect(formatted[1]).to.deep.equal(data)
    expect(formatted[2]).to.equal(data['extra'])
  })

  it('should return an object argument', () => {
    const args = [['id', 'property1']]
    const formatted = service.makeArgs(args, data)
    expect(formatted).to.have.length(1)
    expect(formatted[0]).to.be.instanceof(Object)
    expect(formatted[0]).to.deep.equal({ id: data.id, property1: data.property1 })
  })

  it('should return an object argument and a plain argument', () => {
    const args = [['id', 'property1'], 'name']
    const formatted = service.makeArgs(args, data)
    expect(formatted).to.have.length(2)
    expect(formatted[0]).to.be.instanceof(Object)
    expect(formatted[0]).to.deep.equal({ id: data.id, property1: data.property1 })
    expect(formatted[1]).to.equal(data['name'])
  })

  it('should return an object argument, a plain argument and the whole payload', () => {
    const args = [['id', 'property1'], 'name', '*']
    const formatted = service.makeArgs(args, data)
    expect(formatted).to.have.length(3)
    expect(formatted[0]).to.be.instanceof(Object)
    expect(formatted[0]).to.deep.equal({ id: data.id, property1: data.property1 })
    expect(formatted[1]).to.equal(data['name'])
    expect(formatted[2]).to.deep.equal(data)
  })

  it('should return a nested argument', () => {
    const args = ['nested_object.a']
    const formatted = service.makeArgs(args, data)
    expect(formatted).to.have.length(1)
    expect(formatted[0]).to.equal(data.nested_object.a)
  })

  it('should return undefined because the data is undefined', () => {
    const args = ['nested_object.a']
    const formatted = service.makeArgs(args, undefined)
    expect(formatted).to.have.length(1)
    expect(formatted[0]).to.equal(undefined)
  })

  it('should return undefined because the key does not exist', () => {
    const args = ['nested_object.c']
    const formatted = service.makeArgs(args, undefined)
    expect(formatted).to.have.length(1)
    expect(formatted[0]).to.equal(undefined)
  })

  it('should return a nested argument and a deeply nested argument', () => {
    const args = ['nested_object.a', 'nested_object.another_level.hey']
    const formatted = service.makeArgs(args, data)
    expect(formatted).to.have.length(2)
    expect(formatted[0]).to.equal(data.nested_object.a)
    expect(formatted[1]).to.equal(data.nested_object.another_level.hey)
  })

  it('should return a nested argument and an object with a deeply nested argument', () => {
    const args = ['nested_object.a', [{ key: 'custom_key', value: 'nested_object.another_level.hey' }]]
    const formatted = service.makeArgs(args, data)
    expect(formatted).to.have.length(2)
    expect(formatted[0]).to.equal(data.nested_object.a)
    expect(formatted[1]).to.be.instanceOf(Object).and.have.property('custom_key').equal(data.nested_object.another_level.hey)
  })

  it('should return a nested argument and an object with a deeply nested argument without the set key', () => {
    const args = ['nested_object.a', ['nested_object.another_level.hey']]
    const formatted = service.makeArgs(args, data)
    expect(formatted).to.have.length(2)
    expect(formatted[0]).to.equal(data.nested_object.a)
    expect(formatted[1]).to.be.instanceOf(Object).and.have.property('nested_object.another_level.hey').equal(data.nested_object.another_level.hey)
  })
})

describe('Calls a method through the service', () => {
  it('should allow the execution because its a superuser', async () => {
    const service = new Service(users.super, serviceConfig)
    await expect(service.batchFetch(rawData)).to.eventually.equal('success')
  })

  it('should allow the execution because its the user updating itself', async () => {
    const service = new Service(users.normal1, serviceConfig)
    await expect(service.joinTeam(rawData)).to.eventually.equal('success')
  })

  it('should deny the execution because its a user that is updating another user', async () => {
    const service = new Service(users.normal2, serviceConfig)
    await expect(service.joinTeam(rawData)).to.eventually.be.rejectedWith(Error, 'yourself')
  })

  it('should deny the execution because it doesnt have the correct invite', async () => {
    const service = new Service(users.normal1, serviceConfig)
    await expect(service.joinTeam(rawData2)).to.eventually.be.rejectedWith(Error, 'invite')
  })

  it('should deny the execution because its not the right user and not the right invite', async () => {
    const service = new Service(users.normal2, serviceConfig)
    await expect(service.joinTeam(rawData2)).to.eventually.be.rejectedWith(Error, 'yourself')
  })
})

describe('Checks whether a field can be accessed', () => {
  it('should return the null because the user can not access that property', async () => {
    const service = new Service(users.normal2, serviceConfig)
    await expect(service.protectField('users', rawData, controller.arr())).to.eventually.equal(null)
  })

  it('should return the field because the superuser can access that property', async () => {
    const service = new Service(users.super, serviceConfig)
    await expect(service.protectField('users', rawData, controller.arr())).to.eventually.have.length(2)
  })
})
