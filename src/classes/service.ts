/** ACL Helpers */
import { Is, IsConfig } from './is'

interface ServiceConfigProtectedRule {
  type: string
  message?: string
  exception?: any
}

interface ServiceConfigProtectedMethod {
  name: string
  method: (...args: any[]) => any
  rules?: ServiceConfigProtectedRule[]
  args?: any[]
}

interface ServiceConfigProtectedField {
  [key: string]: ServiceConfigProtectedRule[]
}

interface ServiceConfig {
  is?: IsConfig
  protected?: string[]
  exception?: Error
  methods?: ServiceConfigProtectedMethod[]
  fields?: ServiceConfigProtectedField[]
  resolvers: ServiceResolverobject
}

type ServiceResolver = (is?: Is, user?: object, raw?: object, item?: object) => boolean | Promise<boolean>

interface ServiceResolverobject {
  [key: string]: ServiceResolver
}

/**
 * Provides an easy way to protect methods by accepting rules
 * and checking them against ACLs or custom resolvers.
 * It also sanitizes the input
 */
class Service {
  user: object
  config: ServiceConfig
  is: Is
  defaultResolvers: ServiceResolverobject
  resolvers: ServiceResolverobject

  /**
   * Creates a service instance
   * @param user User data
   * @param config Configuration data
   * @param config.is Configuration data for the IS instance
   * @param config.exception Default exception
   * @param config.methods Methods
   * @param config.fields Fields
   */
  constructor (userObject, configObject) {
    /** User data */
    this.user = userObject
    /** Config data */
    this.config = configObject
    /** Is instance */
    if (this.config.is) {
      this.is = new Is(this.user, this.config.is)
    }
    /** Default resolvers */
    this.defaultResolvers = {
      privileged: async (is) => !is.Privileged()
    }

    /** Merge default resolvers with the passed ones */
    this.config.resolvers = {
      ...this.defaultResolvers,
      ...this.config.resolvers
    }

    /** Register the methods so they can be called as if there was nothing in the middle */
    this.config.methods.forEach((method) => {
      const methodName = typeof method === 'object' ? method.name : method
      /** Dynamically registered method */
      this[methodName] = raw => this.resolve(method, raw)
    })
  }

  /**
   * Checks rules by calling resolvers with request data
   */
  async forbidden (rules: ServiceConfigProtectedRule[], request: object, item?: object) {
    for (const rule of rules) {
      const res = await this.config.resolvers[rule.type](this.is, this.user, request, item)
      if (res) {
        return {
          Exception: rule.exception ? rule.exception : this.config.exception,
          message: rule.message
        }
      }
    }
    return false
  }

  /**
   * Return args to send to the method
   * @param {any[]} args Arguments to send
   * @param {object} data Raw data
   * @param {boolean} object Whether we want to return an object or not
   */
  makeArgs (args: any[] = [], data: object = {}, object: boolean = false): any {
    const objectRes = {}
    const arrayRes = []
    args.forEach(el => {
      let value: any
      let propertyName: string
      if (!Array.isArray(el)) {
        if (!(typeof el === 'string')) {
          propertyName = el.key
          el = el.value
        }
        const splitted = el.split('.')
        if (splitted.length > 1) {
          let latestValue = data
          let i = 0
          do {
            latestValue = latestValue[splitted[i]]
            i += 1
          } while (i < splitted.length && latestValue !== undefined)
          value = latestValue
        } else {
          value = el !== '*' ? data[el] : data
        }
      } else {
        value = this.makeArgs(el, data, true)
      }
      if (!object) {
        arrayRes.push(value)
      } else {
        objectRes[propertyName || el] = value
      }
    })
    if (object) {
      return objectRes
    }
    return arrayRes
  }

  async protectField (field: string, rawRequest: object, rawResult: any | Promise<any>) {
    rawResult = await rawResult
    const payload = {}
    payload[field] = rawResult
    const res = await this.protectOne(rawRequest, payload)
    return res[field] !== null ? rawResult : null
  }

  async protect (rawRequest: object, rawResult: any | Promise<any>) {
    rawResult = await rawResult
    if (!Array.isArray(rawResult)) {
      return this.protectOne(rawRequest, rawResult)
    }
    const result = await Promise.all(rawResult.map(el => this.protectOne(rawRequest, el)))
    const res = result.filter(el => el !== null)
    return res
  }

  async protectOne (rawRequest: object, rawResult: any | Promise<any>) {
    const result = await rawResult
    for (const property in rawResult) {
      if (rawResult.hasOwnProperty(property) && this.config.fields[property]) {
        const forbidden = await this.forbidden(this.config.fields[property], rawRequest, rawResult)
        if (forbidden) {
          result[property] = null
        }
      }
    }
    return result
  }

  /**
   * Resolve a method. If the method is protected: First it will check the
   * request with the rules and then sanitize the input. When all steps are
   * passed successfully, the method gets executed
   * @param {object|string} method Method info
   * @param {object} raw Request data
   * @returns {*} Method response
   * @throws {*} Default or rule error if access is forbidden
   */
  async resolve (method: ServiceConfigProtectedMethod, raw: object) {
    if (method.rules) {
      const forbidden = await this.forbidden(method.rules, raw)
      if (forbidden) {
        throw new forbidden.Exception(forbidden.message)
      }
    }
    let argData: any[]
    raw = raw
    if (method.args) {
      argData = this.makeArgs(method.args, raw)
    } else {
      argData = [raw]
    }
    return method.method(...argData)
  }
}

/** Export */
export default Service
