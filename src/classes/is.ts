export interface IsConfig {
  roleField: string
  privileged: string[]
}

/**
 * Class that provides helpers to determine access level
 */
export class Is {
  private user: object
  private config: IsConfig
  /**
   * Creates an IS instance
   */
  constructor (user: object, config: IsConfig) {
    /** User data */
    this.user = user
    /** Config data */
    this.config = config
  }

  /**
   * Checks whether a role is found in a user
   */
  Role (role?: string) {
    return this.user[this.config.roleField] === role;
  }

  /**
   * Checks whether the user is Privileged or not (Admin or Superuser)
   */
  Privileged () {
    if (!this.config.privileged) {
      return false
    }
    let res = false
    this.config.privileged.forEach(el => { res = res || this.Role(el) })
    return res
  }
}

export default Is
